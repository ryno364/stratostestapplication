package com.example.stratostestapplication

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.stratostestapplication.models.Producer
import com.example.stratostestapplication.parsing.JSONParser
import com.example.stratostestapplication.util.FileUtil

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val propertyDataJsonString = FileUtil.readRawFile(this, R.raw.property_data)

        if (propertyDataJsonString == null) {
            val parentLayout = findViewById<View>(android.R.id.content)

            Snackbar.make(parentLayout, this.resources.getString(R.string.error_cannot_read_file), Snackbar.LENGTH_LONG).show()
            return
        }

        val jsonParser = JSONParser()
        val producerModel = jsonParser.parseToObject<Producer>(propertyDataJsonString)
        if (producerModel == null) {
            val parentLayout = findViewById<View>(android.R.id.content)
            Snackbar.make(parentLayout, this.resources.getString(R.string.error_cannot_parse_json_file), Snackbar.LENGTH_LONG).show()
            return
        }

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = ListMainAdapter(this, producerModel)

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}
