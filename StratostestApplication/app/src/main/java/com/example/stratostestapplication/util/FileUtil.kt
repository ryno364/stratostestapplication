package com.example.stratostestapplication.util

import android.content.Context
import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader
import java.io.Reader

object FileUtil {
    fun readRawFile(context: Context, resId: Int) : String? {
        val inputStream = context.resources.openRawResource(resId)
        val inputReader = InputStreamReader(inputStream)
        val buffReader = BufferedReader(inputReader as Reader)

        val result = buffReader.readText()
        return result
    }
}