package com.example.stratostestapplication.parsing

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi

class JSONParser {

    inline fun <reified T: Any> parseToObject(jsonString: String): T? {
        val moshi = Moshi.Builder().build()
        val jsonAdapter: JsonAdapter<T> = moshi.adapter(T::class.java)

        return jsonAdapter.fromJson(jsonString)
    }
}