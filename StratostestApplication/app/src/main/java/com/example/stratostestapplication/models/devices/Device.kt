package com.example.stratostestapplication.models.devices

interface Device {
    val id: Int
    val adminAccessible: Boolean
    val unit: String
    val model: String
}