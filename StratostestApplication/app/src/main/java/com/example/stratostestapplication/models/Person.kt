package com.example.stratostestapplication.models

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Person(@Json(name = "first_name") val firstName: String,
                  @Json(name = "last_name") val lastName: String,
                  val unit: String,
                  val roles: Array<String>) {
}