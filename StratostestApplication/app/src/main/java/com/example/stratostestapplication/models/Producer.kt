package com.example.stratostestapplication.models

import com.example.stratostestapplication.models.devices.Light
import com.example.stratostestapplication.models.devices.Lock
import com.example.stratostestapplication.models.devices.Thermostat
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter=true)
data class Producer (
    val name: String,
    val address: Address,
    val devices: GroupedDevices,
    val people: Array<Person>) {

    @JsonClass(generateAdapter = true)
    data class Address (@Json(name = "address_line_1") val addressLine1: String,
                        val city: String,
                        val state: String,
                        val zip: String) {

    }

    @JsonClass(generateAdapter = true)
    data class GroupedDevices(val thermostats: Array<Thermostat>,
                              val lights: Array<Light>,
                              val locks: Array<Lock>) {


    }
}


