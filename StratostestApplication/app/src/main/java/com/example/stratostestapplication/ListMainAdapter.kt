package com.example.stratostestapplication

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import com.example.stratostestapplication.models.Person
import com.example.stratostestapplication.models.Producer
import com.example.stratostestapplication.models.devices.Device
import com.example.stratostestapplication.models.devices.Light
import com.example.stratostestapplication.models.devices.Lock
import com.example.stratostestapplication.models.devices.Thermostat
import kotlinx.android.synthetic.main.list_main_devices.view.*
import kotlinx.android.synthetic.main.list_main_people.view.*
import kotlinx.android.synthetic.main.list_main_producer.view.*
import kotlinx.android.synthetic.main.list_main_section.view.*

class ListMainAdapter(val context: Context, val producerModel: Producer) : RecyclerView.Adapter<BaseViewHolder>() {

    private val PRODUCER_VIEW_TYPE = 1
    private val PEOPLE_VIEW_TYPE = 2
    private val DEVICES_VIEW_TYPE = 3
    private val SECTION_VIEW_TYPE = 4

    // The models we are displaying but flattened into an single array for easy retrieval
    private val flatArray: ArrayList<Any>

    init {
        this.flatArray = ArrayList<Any>()
        this.flatArray.add(this.producerModel)

        // We are following the order (thermostats, lights, locks) outlined in the data itself.
        // Don't forget section names.
        this.flatArray.add(SectionDivider(context.resources.getString(R.string.section_name_devices_thermostats)))

        for (thermo in this.producerModel.devices.thermostats) {
            this.flatArray.add(thermo)
        }

        this.flatArray.add(SectionDivider(context.resources.getString(R.string.section_name_devices_lights)))

        for (light in this.producerModel.devices.lights) {
            this.flatArray.add(light)
        }

        this.flatArray.add(SectionDivider(context.resources.getString(R.string.section_name_devices_locks)))

        for (lock in this.producerModel.devices.locks) {
            this.flatArray.add(lock)
        }

        this.flatArray.add(SectionDivider(context.resources.getString(R.string.section_name_people)))

        for (person in this.producerModel.people) {
            this.flatArray.add(person)
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {

        when (holder) {
            is SectionDividerViewHolder -> {
                val sectionDivider = this.flatArray[position] as SectionDivider
                holder.sectionNameTextView.text = sectionDivider.sectionName
            }

            is ProducerViewHolder -> {
                holder.nameTextView.text = this.producerModel.name
                holder.addressLine1TextView.text = this.producerModel.address.addressLine1
                holder.cityTextView.text = this.producerModel.address.city
                holder.stateTextView.text = this.producerModel.address.state
                holder.zipTextView.text = this.producerModel.address.zip
            }

            is PeopleViewHolder -> {
                val person = this.flatArray[position] as Person

                holder.firstNameTextView.text = person.firstName
                holder.lastNameTextView.text = person.lastName
                holder.unitTextView.text = person.unit
                holder.rolesTextView.text = person.roles.joinToString(", ")
            }

            is DevicesViewHolder -> {
                val device = this.flatArray[position] as Device

                holder.modelNameTextView.text = device.model + "_" + device.id.toShort()
                holder.adminAccessibleSwitch.isChecked = device.adminAccessible
                holder.unitTextView.text = device.unit

            }
        }
    }

    override fun getItemViewType(position: Int): Int {

        val item = this.flatArray[position]
        when (item) {
            is SectionDivider -> {
                return SECTION_VIEW_TYPE
            }
            is Producer -> {
                return PRODUCER_VIEW_TYPE
            }
            is Thermostat -> {
                return DEVICES_VIEW_TYPE
            }
            is Light -> {
                return DEVICES_VIEW_TYPE
            }
            is Lock -> {
                return DEVICES_VIEW_TYPE
            }
            is Person -> {
                return PEOPLE_VIEW_TYPE
            }
        }

        return -1
    }

    override fun getItemCount(): Int {
        return this.flatArray.count()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)

        when(viewType) {
            PRODUCER_VIEW_TYPE -> {
                val view = inflater.inflate(R.layout.list_main_producer, parent, false)
                return ProducerViewHolder(view)
            }
            PEOPLE_VIEW_TYPE -> {
                val view = inflater.inflate(R.layout.list_main_people, parent, false)
                return PeopleViewHolder(view)
            }
            DEVICES_VIEW_TYPE -> {
                val view = inflater.inflate(R.layout.list_main_devices, parent, false)
                return DevicesViewHolder(view)
            }
            SECTION_VIEW_TYPE -> {
                val view = inflater.inflate(R.layout.list_main_section, parent, false)
                return SectionDividerViewHolder(view)
            }
        }
        val error = context.resources.getString(R.string.error_invalid_row_type) + viewType
        throw Error(error)
    }
}

// A class representing the current name of the section.  Used only in this file.
class SectionDivider (val sectionName: String) { }


class ProducerViewHolder(itemView: View) : BaseViewHolder(itemView) {

    val nameTextView = itemView.list_main_producer_name
    val addressLine1TextView = itemView.list_main_producer_address_line_1
    val cityTextView = itemView.list_main_producer_city
    val stateTextView = itemView.list_main_producer_state
    val zipTextView = itemView.list_main_producer_zip
}

class DevicesViewHolder(itemView: View) : BaseViewHolder(itemView) {

    val modelNameTextView = itemView.list_main_devices_model_name
    val unitTextView = itemView.list_main_devices_unit
    val adminAccessibleSwitch = itemView.list_main_devices_admin_accessible_switch
}

class PeopleViewHolder(itemView: View) : BaseViewHolder(itemView) {
    val firstNameTextView = itemView.list_main_people_first_name
    val lastNameTextView = itemView.list_main_people_last_name
    val unitTextView = itemView.list_main_people_unit
    val rolesTextView = itemView.list_main_people_roles
}

class SectionDividerViewHolder(itemView: View) : BaseViewHolder(itemView) {
    val sectionNameTextView = itemView.list_main_section_name
}

open class BaseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) { }