package com.example.stratostestapplication.models.devices

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Thermostat (@Json(name = "id") val rawId: Int,
                       @Json(name = "unit") val rawUnit: String,
                       @Json(name = "model") val rawModel: String,
                       @Json(name = "admin_accessible") val rawAdminAccessible: String) : Device {

    override val id: Int
        get() = this.rawId

    override val unit: String
        get() = this.rawUnit

    override val model: String
        get() = this.rawModel

    override val adminAccessible: Boolean
        get() = this.rawAdminAccessible.toBoolean()
}