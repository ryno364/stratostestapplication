package com.example.stratostestapplication

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import com.example.stratostestapplication.util.FileUtil



import androidx.test.platform.app.InstrumentationRegistry
import com.example.stratostestapplication.models.Producer
import com.example.stratostestapplication.parsing.JSONParser


import org.junit.Assert.*
import org.junit.*
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(RobolectricTestRunner::class)
class JSONFileUnitTest {

    lateinit var context: Context

    @Before
    fun setup() {
        context = ApplicationProvider.getApplicationContext()
    }

    /**
     * Make sure our test environment is setup and ready to go.
     */
    @Test
    fun checkContextIsValid() {
        assertNotNull(context)
    }


    /**
     * Make sure we can read the JSON file itself.
     */
    @Test
    fun readJSONFileTest() {
        val propertyDataJsonString = FileUtil.readRawFile(context, R.raw.property_data)
        assertNotNull(propertyDataJsonString)
    }

    /**
     * Make sure the JSON objects within the JSON file are in the correct json format.
     */
    @Test
    fun parseJSONFileTest() {

        val propertyDataJsonString = FileUtil.readRawFile(context, R.raw.property_data)

        val jsonParser = JSONParser()
        val producerModel = jsonParser.parseToObject<Producer>(propertyDataJsonString!!)

        // If the parser fails, then the json is not in the correct format.
        assertNotNull(producerModel == null)
    }

    /**
     * Make sure the JSON objects within the JSON file have the correct number of models.
     * In a live data driven environment, this kind of test would usually not be needed on the client side.
     */
    @Test
    fun expectedJSONFileObjectContentsCount() {
        val propertyDataJsonString = FileUtil.readRawFile(context, R.raw.property_data)
        val jsonParser = JSONParser()
        val producerModel = jsonParser.parseToObject<Producer>(propertyDataJsonString!!)

        assert(producerModel?.devices?.locks?.count() == 5)
        assert(producerModel?.devices?.lights?.count() == 8)
        assert(producerModel?.devices?.thermostats?.count() == 5)
        assert(producerModel?.people?.count() == 6)

    }
}
